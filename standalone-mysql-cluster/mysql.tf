terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
    }

    helm = {
      source = "hashicorp/helm"
    }
  }

  required_version = ">= 0.13"
}

variable "do_token" {}

module "load_testing_host" {
  source = "../load-testing-host"
  do_token = var.do_token
}

provider "digitalocean" {
  token = var.do_token
}

provider "helm" {
  kubernetes {
    host                   = digitalocean_kubernetes_cluster.standalone_mysql_cluster.endpoint
    token                  = digitalocean_kubernetes_cluster.standalone_mysql_cluster.kube_config[0].token
    cluster_ca_certificate = base64decode(
      digitalocean_kubernetes_cluster.standalone_mysql_cluster.kube_config[0].cluster_ca_certificate
    )
  }
}

resource "digitalocean_kubernetes_cluster" "standalone_mysql_cluster" {
  name    = "standalone-mysql-cluster"
  region  = "sgp1"
  version = "1.19.3-do.3"

  node_pool {
    name       = "main-pool"
    size       = "s-2vcpu-4gb"
    node_count = 1
  }
}

resource "helm_release" "mysql" {
  name       = "mysql"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "mysql"
  version    = "8.2.5"
  values     = [file("standalone-mysql-cluster/mysql-values.yaml")]
}
