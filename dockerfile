FROM golang:1.15-alpine as build
WORKDIR /build
RUN apk --no-cache add git gcc musl-dev
RUN go get -u github.com/k6io/xk6/cmd/xk6
RUN CGO_ENABLED=1 xk6 build master --with github.com/imiric/xk6-sql

FROM alpine:3.11
WORKDIR /app
COPY --from=build /build/k6 /usr/bin/k6

ENTRYPOINT ["k6"]

