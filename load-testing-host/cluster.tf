terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
    }
  }

  required_version = ">= 0.13"
}

variable "do_token" {}

provider "digitalocean" {
  token = var.do_token
}

resource "digitalocean_kubernetes_cluster" "load_testing_host_cluster" {
  name    = "load-testing-host-cluster"
  region  = "sgp1"
  version = "1.19.3-do.3"

  node_pool {
    name       = "main-pool"
    size       = "s-4vcpu-8gb"
    node_count = 1
  }
}
