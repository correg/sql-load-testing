import sql from 'k6/x/sql';
import { Counter, Rate } from 'k6/metrics'
import { fail } from 'k6'

const db = sql.open("sqlite3", "./test.db");

const queryCount = new Counter('query_count')
const queryErrorCount = new Counter('query_error_count')
const queryCompleteRate = new Rate('query_complete_rate')

export let options = {
  stages: [
    { duration: '15s', target: 100 }, // below normal load
  ]
};

export function setup() {
  db.exec(`CREATE TABLE IF NOT EXISTS keyvalues (
    id integer PRIMARY KEY AUTOINCREMENT,
    key varchar NOT NULL,
    value varchar);`);
  
  db.exec(`
    CREATE TABLE IF NOT EXISTS tbl_onl (
      semester char(1) NOT NULL,
      year char(4) NOT NULL,
      student_id char(9) NOT NULL,
      courseno char(6) NOT NULL,
      seclec char(3) NOT NULL,
      seclab char(3) NOT NULL,
      regist_type char(1) NOT NULL,
      crelec decimal(4,2) DEFAULT NULL,
      crelab decimal(4,2) DEFAULT NULL,
      seclec_move char(3) DEFAULT NULL,
      seclab_move char(3) DEFAULT NULL,
      ip varchar(20) DEFAULT NULL,
      input_user varchar(50) DEFAULT NULL,
      input_date datetime DEFAULT CURRENT_TIMESTAMP,
      random_no int(11) DEFAULT NULL,
      reason_id char(2) DEFAULT NULL,
      rank_no int(2) DEFAULT '99'
    );
  `)
}

export function teardown() {
  // db.exec("INSERT INTO keyvalues (key, value) VALUES('plugin-name', 'k6-plugin-sql');");

  let results = sql.query(db, "SELECT COUNT(*) FROM tbl_onl;");
  results.forEach(row => console.log('result: ', row['COUNT(*)']))

  db.close();
}

const randomCourse = () => '000' + Math.random().toString().slice(-3)

const randomStudentId = () => Math.random().toString().slice(-9)

export default function () {

  try {
    const course = randomCourse()
    const studentId = randomStudentId()

    db.exec(`INSERT INTO tbl_onl (semester, year, student_id, courseno, seclec, seclab, regist_type)
      VALUES('1', '2020', '${studentId}', '${course}', '001', '000', 'A');`)
    queryCount.add(1)
    queryCompleteRate.add(true)
  } catch (e) {
    // console.log(e)
    queryErrorCount.add(1)
    queryCompleteRate.add(false)
    fail(e)
  }
}
