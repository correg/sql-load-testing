build: 
	docker build . -t k6-sql:0.0.1
push-code:
	kubectl delete configmap sql-load-files
	kubectl create configmap sql-load-files --from-file=sql.js
create-job:
	kubectl apply -f job.yaml
clean:
	kubectl delete -f job.yaml
plan-infra:
	terraform plan -var-file pf.tfvars load-testing-host
apply-infra:
	terraform apply -var-file pf.tfvars load-testing-host
clean-infra:
	terraform destroy -var-file pf.tfvars load-testing-host
plan-standalone-mysql:
	terraform plan -var-file pf.tfvars standalone-mysql-cluster
apply-standalone-mysql:
	terraform apply -var-file pf.tfvars standalone-mysql-cluster
clean-standalone-mysql:
	terraform destroy -var-file pf.tfvars standalone-mysql-cluster
